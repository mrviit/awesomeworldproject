TEMPLATE = aux
QT_INSTALL_FRAMEWORK_PATH = C:/Qt/Tools/QtInstallerFramework/3.0

DESTDIR = $$PWD/../AwesomeWorld-Installer/packages/ics.component/data

create_package.commands = $$quote(windeployqt --qmldir $$PWD/../AwesomeWorld $${DESTDIR})

QMAKE_EXTRA_TARGETS += create_package
PRE_TARGETDEPS  += create_package

DISTFILES += \
    config/config.xml \
    packages/ics.component/meta/package.xml \
    packages/ics.component/meta/installscript.qs

INSTALLER = awesomeWorld-installer

INPUT = $$PWD/config/config.xml $$PWD/packages

awesome-installer.input = INPUT
awesome-installer.output = $$INSTALLER
awesome-installer.commands = $$QT_INSTALL_FRAMEWORK_PATH/bin/binarycreator --offline-only -c $$PWD/config/config.xml -p $$PWD/packages ${QMAKE_FILE_OUT}
awesome-installer.CONFIG += target_predeps no_link combine
win32 {
    awesome-installer.clean_commands += $$quote(del ..\..\AwesomeWorldProject\AwesomeWorld-Installer\packages\ics.component\data /Q)
    awesome-installer.clean_commands += & for /d %%x in (..\..\AwesomeWorldProject\AwesomeWorld-Installer\packages\ics.component\data\*) do rd /s /q "%%x"
}


QMAKE_EXTRA_COMPILERS += awesome-installer
